awk '# miningrep - individual jenkins log
# load team jenkins log
# jenkinsログから予め辞書を作る
# バリューがビルド結果：etnry[1]
# キーは時，分，秒
FILENAME == jenkinslog {
	split($0, entry, "\ ")
	split(entry[6], time, ":")
	buildresult[entry[5], time[1], time[2]+=0] = entry[1]
	buildresult2[entry[5], time[1], time[2], time[3]] = entry[1]
	next
}

BEGIN {FS="|"}
$2 ~ /^$/{
	next
}

# 辞書buildresultとsvnのログを用いて，
# 個人のビルド成功率を出力する
# 辞書のキーにsvnのコミット時間をいれるとビルドの結果が返ってくる仕組み
# jenkinsとsvnの連携は時差があるので，時差を考慮したif文が書かれている
# if文が対応できるのは，svnログ 17:30:59 かつ jenkinsログ 17:31:00のような分を1分またいだケースのみ
{
	split($3, datetime, "\ ")
	split(datetime[2], time, ":")
	gsub(/-/,"/",datetime[1])
	status = buildresult[datetime[1] ,time[1], time[2]+=0]
	if(status == ""){
		status = buildresult[datetime[1] ,time[1], time[2]+1]
		print $2, status, datetime[1], time[1], time[2]
		print time[2]+1
	}
	else{
		print $2, status, datetime[1], time[1], time[2]
	}
}' jenkinslog=$1 $1 $2 
