awk '# totaljenkins - individual count of jenkins build succsess

BEGIN {FS="\ "
}
$2 ~ /成功/{
	buildsuccsess[$1]++
}
$2 ~ /不安定/{
	next
}
{
	memberlist[$1]=$1
	buildcount[$1]++
}
END{
	for(member in memberlist){
		succsessrate = buildsuccsess[member] / buildcount[member]
		print member, succsessrate
	}
}' $1
