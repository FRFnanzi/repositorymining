awk '
# total reoprt
# input: trac tcikets 
# $13 is totalhour, $15 is reporter

# input and output header
BEGIN{
	FS=", "; OFS=", "; print "id", "team", "codinghour", "bugcount",
	"reviewcount", "bugtime", "reviewtime"
}

# skip record
$1 ~ /id/ || $3 ~ /new/ || $8 ~ /invalid/ || $4 ~ /"" /{
	next
}

# create reporterlist for END
{
	rl[$15]=$15
	gsub(/[^0-9]/,"",FILENAME)
	teamnum[$15]=FILENAME
}

# codinghour increment
$5 ~ /作成（(ソースコード|単体テスト)）/{
	codinghour[$15]+=$13 
}

# total bug fix
$5 ~ /バグ修正（(ソースコード|単体テスト)）/{
	bugcount[$15]++
	bugtime[$15]+=$13
}

# total review
$5 ~ /レビュー/{
	reviewcount[$15]++
	reviewtime[$15] += $13
}

# result print
END{
	for(item in rl){
		if (bugcount[item] == ""){
			bugcount[item] = 0
			bugtime[item] = 0
		 }
		print item, teamnum[item], codinghour[item], bugcount[item],
		reviewcount[item], bugtime[item], reviewtime[item]
	}
}' $*
