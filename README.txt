total jenkins build success rate
tjb.shはjenkinsのビルドステータスとsvnのコミットログで
誰がいつビルドの成功失敗をしたかを表すファイルを元に，
その成功率を個人単位で出力する．


実行方法:
> sh tjb.sh <output cjs.sh>


——————8<——————8<——————8<——————8<——————8<——————8<

connect jenkins log to svn log revision
cjs.shはtjb.shの対象ファイルを作成するための中間スクリプトです．
ある個人のjenkinsビルド成功率をとりたいのだが，
jenkinsのログにはビルドのステータスとそのステータスをとった時間が
記録されているが，誰が実行したかの記録は無い．そこで，誰がビルドした
かを特定するためにsubversionのコミットログから時間をキーにして特定する．

実行方法
> sh cjs.sh <jenkinslog> <svnlog>
